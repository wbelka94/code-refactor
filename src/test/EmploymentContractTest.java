package test;

import com.bartoszwalter.students.taxes.EmploymentContract;
import org.junit.jupiter.api.Test;

import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;

class EmploymentContractTest {

    @Test
    void calculate() {
        EmploymentContract employmentContract = new EmploymentContract(2000);
        employmentContract.calculate();
        Map<String, Double> results = employmentContract.getData();
        assertEquals(new Double(1459.478), results.get("salary"));
    }
}