package test;

import com.bartoszwalter.students.taxes.CivilContract;

import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;

class CivilContractTest {

    @org.junit.jupiter.api.BeforeEach
    void setUp() {
    }

    @org.junit.jupiter.api.AfterEach
    void tearDown() {
    }

    @org.junit.jupiter.api.Test
    void calculate() {
        CivilContract civilContract = new CivilContract(2000);
        civilContract.calculate();
        Map<String, Double> results = civilContract.getData();
        assertEquals(new Double(1455.478), results.get("salary"));
    }
}