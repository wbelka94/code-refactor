package com.bartoszwalter.students.taxes;

import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.Map;

public class ConsoleOutput implements TaxCalculatorOutput {
    @Override
    public void showResults(Map<String,Double> data) {
        for (Map.Entry<String, Double> entry : data.entrySet()) {
            System.out.println(entry.getKey() + ": " + entry.getValue());
        }

        System.out.println("Podstawa wymiaru sk?adek " + data.get("base"));
        System.out.println("Sk?adka na ubezpieczenie emerytalne " + data.get("pensionContribution"));
        System.out.println("Sk?adka na ubezpieczenie rentowe    " + data.get("annuityContribution"));
        System.out.println("Sk?adka na ubezpieczenie chorobowe  " + data.get("sicknessContribution"));
        //Podstawa wymiaru sk?adki na ubezpieczenie zdrowotne:
        System.out.println("Sk?adka na ubezpieczenie zdrowotne: 9% = " + data.get("healthCareContribution1") +
                " 7,75% = " + data.get("healthCareContribution2"));
        System.out.println("Koszty uzyskania przychodu (sta?e) " + data.get("costOfEarning"));
        //Podstawa opodatkowania
        System.out.println("Zaliczka na podatek dochodowy 18 % = " + data.get("taxPrepayment"));
        //Podatek potr?cony =
        System.out.println("Zaliczka do urz?du skarbowego = " + data.get("incomeTaxPrepayment") +
                " po zaokr?gleniu = " + data.get("incomeTaxPrepayment0"));
        System.out.println("Pracownik otrzyma wynagrodzenie netto w wysoko?ci = " + data.get("salary"));

        System.out.println("" + data.get("withholdingTax"));

        System.out.println("" + data.get("taxReducingAmount"));
    }
}