package com.bartoszwalter.students.taxes;

import java.text.DecimalFormat;

public class EmploymentContract extends Contract {
    public EmploymentContract(double base) {
        super(base);
    }

    @Override
    public void calculate() {
        double taxBase = calculateBase();
        calculateHealthCareInsurance(taxBase);
        double podstawaOpodat = taxBase - costOfEarning;
        calculateTax(podstawaOpodat);
        withholdingTax = taxPrepayment - taxReducingAmount;
        calculatePrepayment();
        incomeTaxPrepayment0 = Math.round(incomeTaxPrepayment);
        salary = base - ((pensionContribution + annuityContribution + sicknessContribution) + healthCareContribution1 + incomeTaxPrepayment0);
    }
}
