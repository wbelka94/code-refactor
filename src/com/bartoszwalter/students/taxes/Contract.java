package com.bartoszwalter.students.taxes;

import java.util.HashMap;
import java.util.Map;

public abstract class Contract {

    protected double base = 0;
    // składki na ubezpieczenia społeczne
    protected double pensionContribution = 0; // 9,76% podstawyy
    protected double annuityContribution = 0; // 1,5% podstawy
    protected double sicknessContribution = 0; // 2,45% podstawy
    // składki na ubezpieczenia zdrowotne
    protected double costOfEarning = 111.25;
    protected double healthCareContribution1 = 0; // od podstawy wymiaru 9%
    protected double healthCareContribution2 = 0; // od podstawy wymiaru 7,75 %
    protected double taxPrepayment = 0; // zaliczka na podatek dochodowy 18%
    protected double taxReducingAmount = 46.33; // kwota zmienjszająca podatek 46,33 PLN
    protected double incomeTaxPrepayment = 0;
    protected double incomeTaxPrepayment0 = 0;
    protected double salary;
    protected double withholdingTax = 0;

    Contract(double base){
        this.base = base;
    }

    public abstract void calculate();

    public Map<String,Double> getData(){
        HashMap data = new HashMap<String,Double>();
        data.put("salary",this.salary);
        data.put("withholdingTax",this.withholdingTax);
        data.put("incomeTaxPrepayment0",this.incomeTaxPrepayment0);
        data.put("incomeTaxPrepayment",this.incomeTaxPrepayment);
        data.put("taxReducingAmount",this.taxReducingAmount);
        data.put("taxPrepayment",this.taxPrepayment);
        data.put("healthCareContribution2",this.healthCareContribution2);
        data.put("healthCareContribution1",this.healthCareContribution1);
        data.put("costOfEarning",this.costOfEarning);
        data.put("sicknessContribution",this.sicknessContribution);
        data.put("annuityContribution",this.annuityContribution);
        data.put("pensionContribution",this.pensionContribution);
        data.put("base",this.base);
        return data;
    }

    protected double calculateBase() {
        pensionContribution = (base * 9.76) / 100;
        annuityContribution = (base * 1.5) / 100;
        sicknessContribution = (base * 2.45) / 100;
        return (base - pensionContribution - annuityContribution - sicknessContribution);
    }

    protected void calculateHealthCareInsurance(double taxBase) {
        healthCareContribution1 = (taxBase * 9) / 100;
        healthCareContribution2 = (taxBase * 7.75) / 100;
    }

    protected void calculatePrepayment() {
        incomeTaxPrepayment = taxPrepayment - healthCareContribution2 - taxReducingAmount;
    }

    protected void calculateTax(double taxBase) {
        taxPrepayment = (taxBase * 18) / 100;
    }

}
