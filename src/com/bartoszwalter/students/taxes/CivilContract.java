package com.bartoszwalter.students.taxes;

import java.text.DecimalFormat;

public class CivilContract extends Contract {
    public CivilContract(double base) {
        super(base);
    }

    @Override
    public void calculate() {
        double taxBase = calculateBase();
        calculateHealthCareInsurance(taxBase);
        taxReducingAmount = 0;
        costOfEarning = (taxBase * 20) / 100;
        calculateTax(taxBase - costOfEarning);
        withholdingTax = taxPrepayment;
        calculatePrepayment();
        incomeTaxPrepayment0 = Math.round(incomeTaxPrepayment);
        salary = base - ((pensionContribution + annuityContribution + sicknessContribution) + healthCareContribution1 + incomeTaxPrepayment0);
    }
}
