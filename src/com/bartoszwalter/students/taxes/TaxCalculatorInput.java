package com.bartoszwalter.students.taxes;

import java.io.IOException;
import java.util.List;

public interface TaxCalculatorInput {
    double getBase() throws IOException;
    char getContractType(List<String> availableTypes) throws IOException;
}
