package com.bartoszwalter.students.taxes;

import java.util.Map;

public interface TaxCalculatorOutput {
    void showResults(Map<String,Double> data);
}
