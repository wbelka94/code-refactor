package com.bartoszwalter.students.taxes;

import java.util.ArrayList;

public class TaxCalculator {

    public static void main(String[] args) {
        TaxCalculatorInput taxCalculatorInput = new ConsoleInput();
        TaxCalculatorOutput taxCalculatorOutput = new ConsoleOutput();
        Contract contract = null;
        try {
            char contractType = taxCalculatorInput.getContractType(new ArrayList<>());
            if (contractType == 'P') {
                contract = new EmploymentContract(taxCalculatorInput.getBase());
            } else if (contractType == 'Z') {
                contract = new CivilContract(taxCalculatorInput.getBase());
            } else {
                System.out.println("Nieznany typ umowy!");
            }
            contract.calculate();
        } catch (Exception ex) {
            ex.printStackTrace();
            return;
        }

        taxCalculatorOutput.showResults(contract.getData());
    }
}
