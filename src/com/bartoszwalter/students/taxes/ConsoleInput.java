package com.bartoszwalter.students.taxes;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.List;

public class ConsoleInput implements TaxCalculatorInput {
    private BufferedReader br;

    ConsoleInput(){
        InputStreamReader isr = new InputStreamReader(System.in);
         br = new BufferedReader(isr);
    }

    @Override
    public double getBase() throws IOException {
        System.out.print("Podaj kwotędochodu: ");
        return Double.parseDouble(br.readLine());
    }

    @Override
    public char getContractType(List<String> avaliebleTypes) throws IOException {
        System.out.print("Typ umowy: (P)raca, (Z)lecenie: ");
        return br.readLine().charAt(0);
    }
}
